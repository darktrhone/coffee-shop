/** 
 * ===================================================================
 * main js
 *
 * ------------------------------------------------------------------- 
 */

(function ($) {

    "use strict";

    /*---------------------------------------------------- */
    /* Preloader
	------------------------------------------------------ */
    $(window).load(function () {

        // will first fade out the loading animation 
        $("#loader").fadeOut("slow", function () {

            // will fade out the whole DIV that covers the website.
            $("#preloader").delay(300).fadeOut("slow");

        });

    })


    /*----------------------------------------------------*/
    /*	Sticky Navigation
	------------------------------------------------------*/
    $(window).on('scroll', function () {

        var y = $(window).scrollTop(),
		    topBar = $('header');

        if (y > 1) {
            topBar.addClass('sticky');
        }
        else {
            topBar.removeClass('sticky');
        }

    });


    /*-----------------------------------------------------*/
    /* Mobile Menu
   ------------------------------------------------------ */
    var toggleButton = $('.menu-toggle'),
        nav = $('.main-navigation');

    toggleButton.on('click', function (event) {
        event.preventDefault();

        toggleButton.toggleClass('is-clicked');
        nav.slideToggle();
    });

    if (toggleButton.is(':visible')) nav.addClass('mobile');

    $(window).resize(function () {
        if (toggleButton.is(':visible')) nav.addClass('mobile');
        else nav.removeClass('mobile');
    });

    $('#main-nav-wrap li a').on("click", function () {

        if (nav.hasClass('mobile')) {
            toggleButton.toggleClass('is-clicked');
            nav.fadeOut();
        }
    });


    /*----------------------------------------------------*/
    /* Highlight the current section in the navigation bar
  	------------------------------------------------------*/
    var sections = $("section"),
	navigation_links = $("#main-nav-wrap li a");

    if (window.location.pathname == "/") {


        sections.waypoint({

            handler: function (direction) {

                var active_section;

                active_section = $('section#' + this.element.id);

                if (direction === "up") active_section = active_section.prev();

                var active_link = $('#main-nav-wrap a[href="#' + active_section.attr("id") + '"]');

                navigation_links.parent().removeClass("current");
                active_link.parent().addClass("current");

            },

            offset: '25%'

        });

    }
    /*----------------------------------------------------*/
    /* Flexslider
  	/*----------------------------------------------------*/
    $(window).load(function () {

        $('#testimonial-slider').flexslider({
            namespace: "flex-",
            controlsContainer: "",
            animation: 'slide',
            controlNav: true,
            directionNav: true,
            smoothHeight: true,
            slideshowSpeed: 7000,
            animationSpeed: 600,
            randomize: false,
            touch: true,
        });

    });


    /*----------------------------------------------------*/
    /* Smooth Scrolling
  	------------------------------------------------------*/
    $('.smoothscroll').on('click', function (e) {

        e.preventDefault();

        var target = this.hash,
            $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 800, 'swing', function () {
            window.location.hash = target;
        });

    });


    /*----------------------------------------------------*/
    /*  Placeholder Plugin Settings
	------------------------------------------------------*/

    $('input, textarea, select').placeholder()


    /*---------------------------------------------------- */
    /* ajaxchimp
     ------------------------------------------------------ */

    // Example MailChimp url: http://xxx.xxx.list-manage.com/subscribe/post?u=xxx&id=xxx
    var mailChimpURL = 'http://facebook.us8.list-manage.com/subscribe/post?u=cdb7b577e41181934ed6a6a44&amp;id=e65110b38d'

    $('#mc-form').ajaxChimp({

        language: 'es',
        url: mailChimpURL

    });

    // Mailchimp translation
    //
    //  Defaults:
    //	 'submit': 'Submitting...',
    //  0: 'We have sent you a confirmation email',
    //  1: 'Please enter a value',
    //  2: 'An email address must contain a single @',
    //  3: 'The domain portion of the email address is invalid (the portion after the @: )',
    //  4: 'The username portion of the email address is invalid (the portion before the @: )',
    //  5: 'This email address looks fake or invalid. Please enter a real email address'

    $.ajaxChimp.translations.es = {
        'submit': 'Submitting...',
        0: '<i class="fa fa-check"></i> We have sent you a confirmation email',
        1: '<i class="fa fa-warning"></i> You must enter a valid e-mail address.',
        2: '<i class="fa fa-warning"></i> E-mail address is not valid.',
        3: '<i class="fa fa-warning"></i> E-mail address is not valid.',
        4: '<i class="fa fa-warning"></i> E-mail address is not valid.',
        5: '<i class="fa fa-warning"></i> E-mail address is not valid.'
    }


    /*---------------------------------------------------- */
    /* FitVids
	------------------------------------------------------ */
    $(".fluid-video-wrapper").fitVids();


    /*---------------------------------------------------- */
    /*	Modal Popup
	------------------------------------------------------ */

    $('.video-link a').magnificPopup({

        type: 'inline',
        fixedContentPos: false,
        removalDelay: 200,
        showCloseBtn: false,
        mainClass: 'mfp-fade'

    });

    $(document).on('click', '.close-popup', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });


    /*----------------------------------------------------- */
    /* Back to top
   ------------------------------------------------------- */
    var pxShow = 300; // height on which the button will show
    var fadeInTime = 400; // how slow/fast you want the button to show
    var fadeOutTime = 400; // how slow/fast you want the button to hide
    var scrollSpeed = 300; // how slow/fast you want the button to scroll to top. can be a value, 'slow', 'normal' or 'fast'

    // Show or hide the sticky footer button
    jQuery(window).scroll(function () {

        if (!($("#header-search").hasClass('is-visible'))) {

            if (jQuery(window).scrollTop() >= pxShow) {
                jQuery("#go-top").fadeIn(fadeInTime);
            } else {
                jQuery("#go-top").fadeOut(fadeOutTime);
            }

        }

    });

})(jQuery);


function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function setCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";

    document.cookie = name + "=" + value + expires + "; path=/";
}

$(document).ready(function () {
    var loginName = getCookie("login");

    if (loginName !== 'undefined' && loginName != '') {
        $("#WithLogin li:first").html("<a href='javascript:;'>Welcome " + loginName.substr(1) + "</a>");
        $("#WithLogin").attr("style", "display:initial");
    }
    else {
        $("#NoLogin").attr("style", "display:initial");
    }


    $("#logoutButton").on('click', function () {
        setCookie("login", "", "");
        window.location = "/";
    });


    $("#addRecipy").on('click', function () {
        var loginName = getCookie("login");

        if (loginName === 'undefined' || loginName == '') {
            $("#getLogin").click();
        }
        else {
            window.location = "addrecipy";
        }

    });
    $("#allRecipy").on('click', function () {
        var loginName = getCookie("login");

        if (loginName === 'undefined' || loginName == '') {
            $("#getLogin").click();
        }
        else {
            window.location = "AllRecipies";
        }

    });

    $("#registerButton").click(function () {
        var name = $("#RegTitle").val();
        var password = $("#RegPassword").val();
        var cpassword = $("#RegPasswordConfirm").val();
        var picture = $("#RegPicture").val();


        if (name == '' || picture == '' || password == '' || cpassword == '') {
            $("#regErrorMsgs").text("* Please fill all fields.");
            $("#regErrorMsgs").parent().attr("style", "display: initial; width: 100%; text-align: center");
            $("#regSuccMsgs").parent().attr("style", "display: none; width: 100%; text-align: center");
            return;
        }

        if (!(password).match(cpassword)) {
            $("#regErrorMsgs").text("* Your passwords dont match.");
            $("#regErrorMsgs").parent().attr("style", "display: initial; width: 100%; text-align: center");
            $("#regSuccMsgs").parent().attr("style", "display: none; width: 100%; text-align: center");
            return;
        }

        $.ajax({
            type: "GET",
            url: "/Ajax?op=Register",
            data: $("#registerForm").serialize(), // serializes the form's elements.
            success: function (data) {

                if (data == "False") {
                    $("#regErrorMsgs").text("* It was not possible to register, try again later.");
                    $("#regErrorMsgs").parent().attr("style", "display: initial; width: 100%; text-align: center");
                    $("#regSuccMsgs").parent().attr("style", "display: none; width: 100%; text-align: center");
                }
                if (data == "UsernameExists") {
                    $("#regErrorMsgs").text("* Username already exists, try another one.");
                    $("#regErrorMsgs").parent().attr("style", "display: initial; width: 100%; text-align: center");
                    $("#regSuccMsgs").parent().attr("style", "display: none; width: 100%; text-align: center");
                }
                if (data == "True") {
                    window.location = window.location;
                }
            }

        });
    });

    $("#loginButton").click(function () {
        var name = $("#Username").val();
        var password = $("#Password").val();


        if (name == '' || password == '') {
            $("#errorMsgs").text("* Please fill all fields.");
            $("#errorMsgs").parent().attr("style", "display: initial; width: 100%; text-align: center");
            return;
        }

        $.ajax({
            type: "GET",
            url: "/Ajax?op=Login",
            data: $("#loginForm").serialize(), // serializes the form's elements.
            success: function (data) {

                if (data == "False") {
                    $("#errorMsgsRecipy").text("* It was not possible to login, try again later.");
                    $("#errorMsgs").parent().attr("style", "display: initial; width: 100%; text-align: center");
                    $("#succMsgs").parent().attr("style", "display: none; width: 100%; text-align: center");
                }
                if (data == "LoginMismatch") {
                    $("#errorMsgs").text("* Credentials do not match, try again.");
                    $("#errorMsgs").parent().attr("style", "display: initial; width: 100%; text-align: center");
                    $("#succMsgs").parent().attr("style", "display: none; width: 100%; text-align: center");
                }
                
                if (data == "True") {
                    window.location = window.location;
                }
            }

        });


    });

    $("#addRecipyButton").click(function () {
        var title = $("#TitleRec").val();
        var lead = $("#LeadRec").val();
        var description = $("#DescriptionRec").val();
        var img = $("#ImgRec").val();

        if (title == '' || description == '') {
            $("#errorMsgsRecipy").text("* Please fill in the title and description.");
            $("#errorMsgsRecipy").parent().attr("style", "display: initial; width: 100%; text-align: center");
            $("#successMsgsRecipy").parent().attr("style", "display: none; width: 100%; text-align: center");
            return;
        }

        $.ajax({
            type: "GET",
            url: "/Ajax?op=CreateRecipy",
            data: $("#AddRecipyForm").serialize(), // serializes the form's elements.
            success: function (data) {

                if (data == "False") {
                    $("#errorMsgs").text("* It was not possible to add your recipy, try again later.");
                    $("#errorMsgsRecipy").parent().attr("style", "display: initial; width: 100%; text-align: center");
                    $("#successMsgsRecipy").parent().attr("style", "display: none; width: 100%; text-align: center");
                }
                if (data == "True") {
                    $("#errorMsgsRecipy").parent().attr("style", "display: none; width: 100%; text-align: center");
                    $("#successMsgsRecipy").parent().attr("style", "display: initial; width: 100%; text-align: center");
                    $("#AddRecipyForm").find("input[type=text], textarea").val("")
                }
            }

        });
    });
});
