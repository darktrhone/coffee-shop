﻿using DSCoffeeShopTableAdapters;
using System;
using System.Web.UI;

public partial class _default : Page
{

    RecipiesTableAdapter RecipiesTableAdapter;
    DSCoffeeShop.RecipiesDataTable CSRecipiesTable;
    DSCoffeeShop.UsersDataTable CSUsersTable;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            RecipiesTableAdapter = new RecipiesTableAdapter();
            CSRecipiesTable = new DSCoffeeShop.RecipiesDataTable();
            RecipiesTableAdapter.FillByRecipiesAndUsers(CSRecipiesTable);
            RpLatestRecipies.DataSource = this.RecipiesTableAdapter.GetFullData();
            RpLatestRecipies.DataBind();
        }
    }

}