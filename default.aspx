﻿<%@ Page MasterPageFile="~/Masterpage.master" Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="_default" %>


<asp:Content ContentPlaceHolderID="cphMenu" runat="server">
    <nav id="main-nav-wrap">
        <ul class="main-navigation">
            <li class="current"><a class="smoothscroll" href="#intro" title="">Home</a></li>
            <li><a class="smoothscroll" href="#process" title="">Brewing</a></li>
            <li><a class="smoothscroll" href="#features" title="">Features</a></li>
            <li><a class="smoothscroll" href="#testimonials" title="">Latest Recipies</a></li>
            <li><a class="smoothscroll" href="#pricing" title="">Subscriptions</a></li>

            <li class=""><a id="allRecipy" href="javascript:;" title="">All Recipies</a></li>
            <li class=""><a id="addRecipy" href="javascript:;" title="">Add Recipy</a></li>

            <div id="NoLogin" style="display: none">
                <li class="video-link highlight with-sep"><a id="getLogin" href="#login-popup" title="">Login</a></li>
                <li class="video-link highlight with-sep"><a href="#register-popup" title="">Register</a></li>
            </div>
            <div id="WithLogin" style="display: none">
                <li class="video-link highlight with-sep"></li>
                <li class="video-link highlight with-sep"><a id="logoutButton" href="">Logout</a></li>
            </div>

        </ul>
    </nav>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" runat="server">


    <!-- intro section
   ================================================== -->
    <section id="intro">

        <div class="shadow-overlay"></div>

        <div class="intro-content">
            <div class="row">

                <div class="col-twelve">

                    <div class='video-link'>
                        <a href="#video-popup">
                            <img src="images/play-button.png" alt=""></a>
                    </div>

                    <h5>Hello, welcome to Coffee Shop.</h5>
                    <h1>Our awesome shop will make your life a lot better.</h1>

                    <a class="button stroke smoothscroll" href="#process" title="">Learn More</a>

                </div>

            </div>
        </div>

        <!-- Modal Popup
	   ========================================================= -->

        <div id="video-popup" class="popup-modal mfp-hide">

            <div class="fluid-video-wrapper">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/USpMM-JBkdU" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>

            <a class="close-popup">Close</a>

        </div>
        <!-- /video-popup -->

    </section>
    <!-- /intro -->


    <!-- Process Section
   ================================================== -->
    <section id="process">

        <div class="row section-intro">
            <div class="col-twelve with-bottom-line">

                <h5>Brewing Process</h5>
                <h1>How it works?</h1>

                <p class="lead">Coffee preparation is the process of turning coffee beans into a beverage. The process includes four basic steps:</p>

            </div>
        </div>

        <div class="row process-content">

            <div class="left-side">

                <div class="item" data-item="1">

                    <h5>Roast Beans</h5>

                    <p>Raw coffee beans must be roasted.</p>

                </div>

                <div class="item" data-item="2">

                    <h5>Mix</h5>

                    <p>Ground coffee must then be mixed with hot water for a certain time (brewed).</p>

                </div>

            </div>
            <!-- /left-side -->

            <div class="right-side">

                <div class="item" data-item="3">

                    <h5>Separation</h5>

                    <p>Liquid coffee must be separated from the used grounds.</p>

                </div>

                <div class="item" data-item="4">

                    <h5>Drink</h5>

                    <p>Finally, enjoy your drink! </p>

                </div>

            </div>
            <!-- /right-side -->

            <div class="image-part"></div>

        </div>
        <!-- /process-content -->

    </section>
    <!-- /process-->


    <!-- features Section
   ================================================== -->
    <section id="features">

        <div class="row section-intro">
            <div class="col-twelve with-bottom-line">

                <h5>Features</h5>
                <h1>Great features you'll love.</h1>

            </div>
        </div>

        <div class="row features-content">

            <div class="features-list block-1-3 block-s-1-2 block-tab-full group">

                <div class="bgrid feature">

                    <span class="icon"><i class="icon-window"></i></span>

                    <div class="service-content">

                        <h3 class="h05">Brewing</h3>

                        <p>
                            Learn in detail how to correctly brew your coffee.
                        </p>

                    </div>

                </div>
                <!-- /bgrid -->

                <div class="bgrid feature">

                    <span class="icon"><i class="icon-eye"></i></span>

                    <div class="service-content">
                        <h3 class="h05">Check Recipies</h3>

                        <p>
                            Tired of always brewing the same old way? Check our existing user-uploaded recipies!
                        </p>


                    </div>

                </div>
                <!-- /bgrid -->

                <div class="bgrid feature">

                    <span class="icon"><i class="icon-paint-brush"></i></span>

                    <div class="service-content">
                        <h3 class="h05">Add your own recipy !</h3>

                        <p>
                            Our website allows you to upload your own recipy. Share your love for coffee with other people ! 
                        </p>


                    </div>

                </div>
                <!-- /bgrid -->

            </div>
            <!-- features-list -->

        </div>
        <!-- features-content -->

    </section>
    <!-- /features -->



    <!-- Testimonials Section
   ================================================== -->
    <section id="testimonials">

        <div class="row">
            <div class="col-twelve">
                <h2 class="h01">Hear the latest recipies.</h2>
            </div>
        </div>

        <div class="row flex-container">

            <div id="testimonial-slider" class="flexslider">

                <ul class="slides">

                    <asp:Repeater ID="RpLatestRecipies" runat="server">
                        <ItemTemplate>
                            <li>
                                <div class="testimonial-author">
                                    <img style="width: 250px; height: 250px;" src="<%# DataBinder.Eval(Container.DataItem, "Img") %>" />
                                    <div class="author-info">
                                        <span class="position">Made by: <%# DataBinder.Eval(Container.DataItem, "Username") %></span>
                                    </div>
                                </div>
                                <p>
                                    <%# DataBinder.Eval(Container.DataItem, "Title") %>
                                </p>

                                <p>
                                    <%# DataBinder.Eval(Container.DataItem, "Description") %>
                                </p>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>


                </ul>
                <!-- /slides -->

            </div>
            <!-- /testimonial-slider -->

        </div>
        <!-- /flex-container -->

    </section>
    <!-- /testimonials -->

    <!-- pricing
   ================================================== -->

    <section id="pricing">

        <div class="row section-intro">
            <div class="col-twelve with-bottom-line">

                <h5>Our Pricing</h5>
                <h1>Pick the best plan for you.</h1>

            </div>
        </div>

        <div class="row pricing-content">

            <div class="pricing-tables block-1-4 group">

                <div class="bgrid">

                    <div class="price-block">

                        <div class="top-part">

                            <h3 class="plan-title">Starter</h3>
                            <p class="plan-price"><sup>$</sup>4.99</p>
                            <p class="price-month">Per month</p>

                        </div>

                        <div class="bottom-part">
                            <ul class="features">
                                <li><strong>1</strong> Coffee Magazine</li>
                                <li><strong>1</strong> Coffee Bag Per Month</li>
                                <li><strong>Unlimited</strong> Access</li>
                            </ul>
                            <a class="button large" href="">Get Started</a>
                        </div>

                    </div>

                </div>
                <!-- /price-block -->

                <div class="bgrid">

                    <div class="price-block primary">

                        <div class="top-part" data-info="recommended">

                            <h3 class="plan-title">Standard</h3>
                            <p class="plan-price"><sup>$</sup>9.99</p>
                            <p class="price-month">Per month</p>

                        </div>

                        <div class="bottom-part">

                            <ul class="features">
                                <li><strong>2</strong> Coffee Magazines</li>
                                <li><strong>5</strong> Coffee Bag Per Month</li>
                                <li><strong>Unlimited</strong> Access</li>
                            </ul>

                            <a class="button large" href="">Get Started</a>

                        </div>

                    </div>

                </div>
                <!-- /price-block -->

                <div class="bgrid">

                    <div class="price-block">

                        <div class="top-part">

                            <h3 class="plan-title">Premium</h3>
                            <p class="plan-price"><sup>$</sup>19.99</p>
                            <p class="price-month">Per month</p>

                        </div>

                        <div class="bottom-part">

                            <ul class="features">
                                <li><strong>5</strong> Coffee Magazines</li>
                                <li><strong>10</strong> Coffee Bag Per Month</li>
                                <li><strong>Unlimited</strong> Access</li>
                            </ul>

                            <a class="button large" href="">Get Started</a>

                        </div>

                    </div>

                </div>
                <!-- /price-block -->

                <div class="bgrid">

                    <div class="price-block">

                        <div class="top-part">

                            <h3 class="plan-title">Ultimate</h3>
                            <p class="plan-price"><sup>$</sup>29.99</p>
                            <p class="price-month">Per month</p>

                        </div>

                        <div class="bottom-part">

                            <ul class="features">
                                <li><strong>10</strong> Coffee Magazines</li>
                                <li><strong>30</strong> Coffee Bag Per Month</li>
                                <li><strong>Unlimited</strong> Access</li>
                            </ul>

                            <a class="button large" href="">Get Started</a>

                        </div>

                    </div>

                </div>
                <!-- /price-block -->

            </div>
            <!-- /pricing-tables -->

        </div>
        <!-- /pricing-content -->

    </section>
    <!-- /pricing -->



    <!-- cta
   ================================================== -->
    <section id="cta">

        <div class="row cta-content">

            <div class="col-twelve">

                <h1 class="h01">Get started now.</h1>

            </div>

        </div>
        <!-- /cta-content -->

    </section>
    <!-- /cta -->
</asp:Content>
