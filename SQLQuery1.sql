﻿
drop table recipies
drop table users

-- Create Table Users
create table Users(
IdUser int identity primary key,
Username nvarchar (50) not null,
Password nvarchar(50) not null,
Img nvarchar(250)
);

-- Create Table Recipies
create table Recipies (
IdRecipy int identity primary key,
Title nvarchar(100),
Lead nvarchar(250),
Description nvarchar(MAX),
Img nvarchar (250), 
IdAutor int constraint FkIdUser foreign key references Users(IdUser)
);

-- get user by username and password --
SELECT IdUser FROM Users  WHERE  (Username = @Username) AND (Password = @Password)

-- check username exists --
SELECT COUNT(*) FROM Users where  [Username] = @Username
 
 -- get top 4 newest recipies 
SELECT top(4) R.IdRecipy, R.Title, R.Lead, R.Description, R.Img, u.Img as ImgAutor, u.Username FROM dbo.Recipies R
inner join dbo.Users U on R.IdAutor = U.IdUser order by IdRecipy desc


-- get top 4 newest by each author
select ds.IdRecipy, ds.Title, ds.Description, ds.Img , U1.Img as ImgAutor, U1.Username, U1.IdUser from Users as U1
 cross apply 
     (select top 4 *
      from Recipies R1
      where R1.IdAutor = U1.IdUser
      order by R1.IdRecipy desc) as ds


-- insert in Users
INSERT INTO [dbo].[Users]([Username], [Password], [Img]) VALUES (@Username,@Password,@Img) 


insert into Users values 
('Rui','12345','https://scontent-lhr3-1.xx.fbcdn.net/v/t1.0-9/13001222_1288050674545620_1691106834396622517_n.jpg?oh=c72414f1c1525be13d817f3265db10ab&oe=58DFB7BD');


insert into Users values 
('Daniel','12345','https://scontent-lhr3-1.xx.fbcdn.net/v/t1.0-9/11041534_843957632306246_7005897408007410600_n.jpg?oh=9e5c767b0c1bf0347c697ea1e2da782e&oe=58E1EDF0');
 
 insert into Users values 
('Machado','12345','https://scontent-lhr3-1.xx.fbcdn.net/v/t1.0-9/15826064_1428309573847794_7177813149199227414_n.jpg?oh=f42ebcf320fba161256be952edb7399f&oe=58E90118');
 
 

 

 insert into Recipies values (
'Hot Coffee Masala222',
'This warming, spicy coffee drink contains interesting Asian spices, giving it a chai-like flavor.',
'In a saucepan, bring the water to a boil, and stir in the cumin seeds, star anise pod, and cinnamon stick; reduce heat to a simmer, and cook for 3 minutes, stirring occasionally. Stir in the instant coffee, and allow the drink to simmer gently for 2 more minutes. Pour the drink into mugs, straining out the spices, and stir 1 teaspoon of nonfat dry milk powder and 1 teaspoon of white sugar into each mug.',
'http://images.media-allrecipes.com/userphotos/250x250/00/79/19/791946.jpg',
1
);
 
 insert into Recipies values (
'Mocha Coffee',
'Mocha coffee is extremely easy to make in this recipe. Just add cocoa, sugar and milk to hot coffee and drink!',
'Pour hot coffee into a mug. Stir in cocoa, sugar and milk.',
'http://images.media-allrecipes.com/userphotos/250x250/00/80/87/808772.jpg',
1
);

 insert into Recipies values (
'Holyday Punch 2',
'A great holiday punch of cranberry and pineapple juice, with a punch of almond flavoring.',
'In a large container, mix together sugar, cranberry juice, pineapple juice and almond extract. Refrigerate for 1 day. To serve, pour juice mixture into a punch bowl. Stir in ginger ale.',
'http://images.media-allrecipes.com/userphotos/250x250/00/29/90/299035.jpg',
2
);

 insert into Recipies values (
'Mocha Coffee Cooler',
'Pour cold-brewed coffee over ice cream for a classic Italian treat – affogato!',
'Pour coffee into a blender and add creamer, sugar, cocoa, vanilla extract, and ice. Pour vegetable oil over ice cubes and spray cooking spray into blender. Blend until smooth, about 1 1/2 minutes.',
'http://images.media-allrecipes.com/userphotos/250x250/01/00/41/1004140.jpg',
3
);







 
 --select * from Users

 --select * from Recipies 