﻿<%@ WebHandler Language="C#" Class="Ajax" %>

using System;
using System.Web;
using System.Web.UI;
using DSCoffeeShopTableAdapters;

public class Ajax : Page, IHttpHandler
{

    public override void ProcessRequest(HttpContext context)
    {
        var action = HttpContext.Current.Request.QueryString["op"];

        string output = string.Empty;

        if (!string.IsNullOrEmpty(action))
        {
            switch (action)
            {

                case "Register":
                    {
                        output = this.Register(context);
                        break;
                    }
                case "Login":
                    {
                        output = this.Login(context);
                        break;
                    }
                case "CreateRecipy":
                    {
                        output = this.CreateRecipy(context);
                        break;
                    }


                default: break;
            }
        }

        context.Response.ContentType = "text/plain";
        context.Response.Write(output);
    }



    private string Register(HttpContext current)
    {
        UsersTableAdapter UserTableAdapter;

        try
        {
            var user = HttpContext.Current.Request["RegUsername"];
            var password = HttpContext.Current.Request["RegPassword"];
            var passwordConfirm = HttpContext.Current.Request["RegPasswordConfirm"];
            var img = HttpContext.Current.Request["RegPicture"];


            // validar login 
            if (!string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(passwordConfirm) && !string.IsNullOrEmpty(img))
            {
                if (password == passwordConfirm)
                {
                    // Instantiate object
                    UserTableAdapter = new UsersTableAdapter();

                    // return if user exists
                    if (UserTableAdapter.CheckUsernameExists(user) > 0)
                    {
                        return "UsernameExists";
                    }

                    // Exec Query with parameters
                    var result = UserTableAdapter.Insert(user, password, img);
                    var userId = UserTableAdapter.GetUserByLogin(user, password);


                    // Set Login
                    current.Response.Cookies["login"][""] = user;
                    current.Response.Cookies["userid"][""] = userId.ToString();
                    return "True";
                }
            }
        }
        catch (Exception)
        {
            return "False";
        }
        return "False";
    }


    private string Login(HttpContext current)
    {
        UsersTableAdapter UserTableAdapter;

        try
        {
            var user = HttpContext.Current.Request["Username"];
            var password = HttpContext.Current.Request["Password"];

            // validar login 
            if (!string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(password))
            {
                // Instantiate object
                UserTableAdapter = new UsersTableAdapter();

                // Exec Query with parameters
                var userId = UserTableAdapter.GetUserByLogin(user, password);

                // Check if any match
                if (userId != null && ((int)userId) > 0)
                {
                    current.Response.Cookies["login"][""] = user;
                    current.Response.Cookies["userid"][""] = userId.ToString();
                    return "True";
                }

                return "LoginMismatch";
            }
        }
        catch (Exception)
        {
            return "False";
        }

        return "False";
    }


    private string CreateRecipy(HttpContext current)
    {
        RecipiesTableAdapter RecipiesTableAdapter;

        try
        {
            var Title = current.Request["Title"];
            var ImgLink = current.Request["ImgLink"];
            var Lead = current.Request["Lead"];
            var Description = current.Request["Description"];
            var userCookie = current.Request.Cookies["userid"];
            int? userId = int.Parse(userCookie?.Value?.Substring(1));

            if (userId <= 0 || userId == null)
            {
                return "False";
            }

            if (!string.IsNullOrEmpty(Title) && !string.IsNullOrEmpty(Description))
            {
                // Instantiate object
                RecipiesTableAdapter = new RecipiesTableAdapter();

                // Exec Query with parameters
                var result = RecipiesTableAdapter.InsertRecipy(Title, Lead, Description, ImgLink, userId);

                return result == 1 ? "True" : "False";
            }

            return "False";
        }
        catch (Exception)
        {
            return "False";
        }
    }
}