﻿<%@ Page MasterPageFile="~/Masterpage.master" Language="C#" AutoEventWireup="true" CodeFile="addRecipy.aspx.cs" Inherits="addRecipy" %>




<asp:Content ContentPlaceHolderID="cphMenu" runat="server">
    <nav id="main-nav-wrap">
        <div class="main-navigation">
            <li class="current"><a href="javascript:;" title="">Add Recipy</a></li>
            <div id="WithLogin" style="display: none">
                <li class="video-link highlight with-sep"></li>
                <li class="video-link highlight with-sep"><a id="logoutButton" href="">Logout</a></li>
            </div>
        </div>
    </nav>
</asp:Content>



<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" runat="server">

    <section id="intro">
        <div class="shadow-overlay"></div>
        <div class="intro-content">
            <div class="row">
                <div class="col-twelve">
                    <h5>Add New Recipies</h5>
                    <h1>Here is where you can submit your own recipies.</h1>
                </div>
            </div>
        </div>
    </section>

    <section id="process">
        <form id="AddRecipyForm">
            <div class="col-four tab-1-3 mob-full footer-subscribe" style="float: none; margin: auto;">

                <h4>Recipy</h4>

                <p>Insert your recipy details here.</p>

                <div class="subscribe-form">

                    <input type="text" name="Title" id="TitleRec" class="email" placeholder="Title">

                    <input type="text" name="Lead" class="LeadRec" placeholder="Lead">

                    <input type="text" name="ImgLink" class="ImgRec" placeholder="Image Link">

                    <textarea id="DescriptionRec" style="padding: .6rem 2rem; border-radius: 5px; background: #232933; border: none; width: 100%; font-family: 'raleway-regular', sans-serif; color: #bababa; margin-bottom: 1.8rem;" rows="4" cols="50" name="Description" class="email" placeholder="Description"></textarea>

                    <div style="display: none; width: 100%; text-align: center">
                        <label id="errorMsgsRecipy" style="margin-top: -10px; font-size: 1.2rem; color: red;"></label>
                    </div>

                    <div style="display: none; width: 100%; text-align: center">
                        <label id="successMsgsRecipy" style="margin-top: -10px; font-size: 1.2rem; color: green;">Recipy was submitted.</label>
                    </div>

                    <div style="width: 100%; text-align: center">
                        <input type="button" id="addRecipyButton" style="padding-top: 0px;" value="Submit">
                    </div>



                    <label for="mc-email" class="subscribe-message"></label>


                </div>

            </div>
        </form>
    </section>

</asp:Content>
