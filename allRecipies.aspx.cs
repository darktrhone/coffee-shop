﻿using DSCoffeeShopTableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class allRecipies : System.Web.UI.Page
{

    RecipiesTableAdapter RecipiesTableAdapter;
    DSCoffeeShop.RecipiesDataTable CSRecipiesTable;
    DSCoffeeShop.UsersDataTable CSUsersTable;
    private string CurrentUserID = string.Empty;

    private const string AUTHOR_HTML = @"<div class='row'>  
                                          <div class='card cardAutor'>
                                             <img class='autorImg' src='{0}'/>
                                             <p class='card-text autorName'>
                                                Recipies from:  <br /> <br /> {1}                  
                                             </p>
                                          </div> 
                                        </div>";

    private const string RECIPY_HTML = @"<div class='card'>
                                           <img src='{0}' />
                                           <h5 style='text-transform:none'>{1}</h5>
                                           <p class='card-text'>{2}</p>
                                         </div>  ";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            RecipiesTableAdapter = new RecipiesTableAdapter();
            CSRecipiesTable = new DSCoffeeShop.RecipiesDataTable();
            RpLatestRecipies.DataSource = RecipiesTableAdapter.GetTop4RecipiesByUser();
            RpLatestRecipies.DataBind();
        }
    }


    public string GetHtmlOutput(RepeaterItem Container)
    {
        var Username = DataBinder.Eval(Container.DataItem, "Username") as string;
        var UserImg = DataBinder.Eval(Container.DataItem, "ImgAutor") as string;
        var RecipyTitle = DataBinder.Eval(Container.DataItem, "Title") as string;
        var RecipyDecription = DataBinder.Eval(Container.DataItem, "Description") as string;
        var RecipyImg = DataBinder.Eval(Container.DataItem, "Img") as string;


        // se for um user diferente, aplicar primeiro row de Autor.
        StringBuilder sb = new StringBuilder();

        if (CurrentUserID != Username)
        {
            sb.AppendFormat(AUTHOR_HTML, UserImg, Username);
            CurrentUserID = Username;
        }

        sb.AppendFormat(RECIPY_HTML, RecipyImg, RecipyTitle, RecipyDecription);


        return sb.ToString();
    }
}
