﻿<%@ Page MasterPageFile="~/Masterpage.master" Language="C#" AutoEventWireup="true" CodeFile="allRecipies.aspx.cs" Inherits="allRecipies" %>


<asp:Content ContentPlaceHolderID="cphMenu" runat="server">
    <nav id="main-nav-wrap">
        <div class="main-navigation">

            <li class="current"><a href="javascript:;" title="">All Recipies</a></li>
            <div id="WithLogin" style="display: none">
                <li class="video-link highlight with-sep"></li>
                <li class="video-link highlight with-sep"><a id="logoutButton" href="">Logout</a></li>
            </div>

        </div>
    </nav>
</asp:Content>




<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" runat="server">
    <link rel="stylesheet" href="/css/RecipiesList.css">


    <section id="intro">

        <div class="shadow-overlay"></div>

        <div class="intro-content">
            <div class="row">
                <div class="col-twelve">
                    <h5>All Recipies</h5>
                    <h1>Here is where you can see all the latest recipies!</h1>
                </div>
            </div>
        </div>
    </section>



    <section id="process">
        <div class="container">
            <asp:Repeater ID="RpLatestRecipies" runat="server">
                <ItemTemplate>
                    <%# GetHtmlOutput(Container) %>
                </ItemTemplate>
            </asp:Repeater>

        </div>
    </section>
</asp:Content>
